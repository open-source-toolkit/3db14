# .NET Core 6.0 WebAPI 后端项目框架源码

## 项目简介

本项目是一个基于.NET Core 6.0的Web API后端项目框架源码，旨在提供一个开箱即用的后端开发基础。项目采用RESTful风格设计，集成了多种常用组件和工具，帮助开发者快速搭建高效、可扩展的后端服务。

## 主要特性

- **.NET Core 6.0 WebAPI**：基于最新的.NET Core 6.0框架，提供高性能的Web API服务。
- **仓储+服务+接口封装**：采用仓储模式、服务层和接口层的形式封装业务逻辑，提高代码的可维护性和可扩展性。
- **AOP切面Redis缓存**：基于AOP（面向切面编程）实现Redis缓存，提升系统性能。
- **Swagger API文档**：集成Swagger，自动生成API文档，方便前后端对接。
- **AutoMapper对象映射**：使用AutoMapper处理对象之间的映射，简化数据转换逻辑。
- **AutoFac依赖注入**：采用AutoFac作为依赖注入容器，并支持批量服务注入，简化依赖管理。
- **CORS跨域支持**：内置CORS（跨域资源共享）支持，方便前后端分离开发。
- **JWT自定义策略授权**：封装JWT（JSON Web Token）自定义策略授权，保障API安全。
- **SqlSugar ORM**：接入国产数据库ORM组件SqlSugar，封装数据库操作，简化数据访问层。
- **log4net全局异常处理**：基于log4net实现全局异常处理，记录系统运行日志。
- **自定义全局消息返回格式**：统一API返回格式，方便前端处理。
- **Docker容器化支持**：可配合Docker实现容器化部署，简化环境配置。
- **Jenkins CI/CD**：可配合Jenkins实现持续集成与持续部署，自动化构建和发布流程。
- **Nginx负载均衡**：可配合Nginx实现负载均衡，提升系统高可用性。

## 快速开始

### 环境要求

- .NET Core 6.0 SDK
- SQL Server 数据库
- Redis 缓存服务

### 配置数据库连接字符串

在`appsettings.json`文件中配置SQL Server数据库连接字符串：

```json
{
  "ConnectionStrings": {
    "DefaultConnection": "Server=your_server;Database=your_database;User Id=your_user;Password=your_password;"
  }
}
```

### 运行项目

1. 克隆项目到本地：

   ```bash
   git clone https://github.com/your-repo/your-project.git
   ```

2. 进入项目目录：

   ```bash
   cd your-project
   ```

3. 还原依赖包：

   ```bash
   dotnet restore
   ```

4. 运行项目：

   ```bash
   dotnet run
   ```

5. 访问Swagger文档：

   打开浏览器，访问 `http://localhost:5000/swagger` 查看API文档。

## 项目结构

```
/src
  ├── /YourProject.API            # Web API 项目
  ├── /YourProject.Core           # 核心业务逻辑
  ├── /YourProject.Infrastructure # 基础设施层（仓储、数据库操作等）
  ├── /YourProject.Service        # 服务层
  ├── /YourProject.Tests          # 单元测试项目
  └── /YourProject.Web            # 前端项目（可选）
```

## 贡献指南

欢迎贡献代码！请遵循以下步骤：

1. Fork 本仓库。
2. 创建新的分支 (`git checkout -b feature/your-feature`)。
3. 提交你的更改 (`git commit -am 'Add some feature'`)。
4. 推送到分支 (`git push origin feature/your-feature`)。
5. 创建一个新的 Pull Request。

## 许可证

本项目采用 [MIT 许可证](LICENSE)。

## 联系我们

如有任何问题或建议，请通过 [GitHub Issues](https://github.com/your-repo/your-project/issues) 联系我们。

---

希望本项目能帮助你快速搭建高效的后端服务！